package com.flightApp.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity(name = "users")
public class AuthEntity {

	@Id
	private Integer id;

	private String firstName;

	private String lastName;

	private String role;
	
	@Column(name = "user_name",nullable = false, unique = true)
	private String userName;
	
	private String password;
	
}
